function [imdb,net] = cifar_10_q2(varargin)

% -------------------------------------------------------------------------
% setup to use all the functions
% -------------------------------------------------------------------------
setup;

% -------------------------------------------------------------------------
% prepare the data
% -------------------------------------------------------------------------
imdb = prepare_cifar_10_data();

% -------------------------------------------------------------------------
% Visualizing Training Images
% -------------------------------------------------------------------------
figure(10) ; clf ;
subplot(1,2,1) ;
vl_imarraysc(imdb.images.data(:,:,imdb.images.label==1 & imdb.images.set==1));
axis image off ;
title('Training Images for Cifar 10') ;
% -------------------------------------------------------------------------
% Visualizing Testing Images
% -------------------------------------------------------------------------
subplot(1,2,2) ;
vl_imarraysc(imdb.images.data(:,:,imdb.images.label==1 & imdb.images.set==2)) ;
axis image off ;
title('Testing Images for Cifar 10');

% -------------------------------------------------------------------------
% initialize a CNN architecture for Question 2
% -------------------------------------------------------------------------

net = initializeCIFARCNN(20,30,25) ;

% -------------------------------------------------------------------------
% train and evaluate the CNN
% -------------------------------------------------------------------------

trainOpts.batchSize = 100 ;
trainOpts.numEpochs = 15 ;
trainOpts.continue = true ;
%trainOpts.useGpu = false ;
trainOpts.learningRate = 0.005;
trainOpts.expDir = 'data/cifar-experiment-1' ;
trainOpts = vl_argparse(trainOpts, varargin);
% -------------------------------------------------------------------------
% Take the average image out
% -------------------------------------------------------------------------
imdb.images.data=im2single(imdb.images.data);
imageMean = mean(imdb.images.data(:)) ;
imdb.images.data = imdb.images.data - imageMean ;
imdb.images.label = double(imdb.images.label);
imdb.images.id = double(imdb.images.id);
imdb.images.set = double(imdb.images.set);

% -------------------------------------------------------------------------
% Call training function in MatConvNet
% -------------------------------------------------------------------------
[net,info] = cnn_train(net, imdb, @getBatch, trainOpts);
net.layers(end) = [];
net.imageMean = imageMean;

% -------------------------------------------------------------------------
% Saving the trained model for future use.
% -------------------------------------------------------------------------
save('data/chars-experiment/charscnn.mat', '-struct', 'net');

% -------------------------------------------------------------------------
% Visualizing Filters from the first Convolutional Layer
% -------------------------------------------------------------------------
figure(2) ; clf;
vl_imarraysc(squeeze(net.layers{1}.filters),'spacing',2);
axis equal ;
title('filters in the first layer');



% -------------------------------------------------------------------------
% Get Batch function that gives images in batches to train the CNN
% -------------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch);
im = reshape(im, 32, 32, 3, []);
labels = imdb.images.label(1,batch);

