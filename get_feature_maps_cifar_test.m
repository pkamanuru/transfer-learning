function [testing_feature_maps,testing_labels,training_feature_maps,training_labels] = get_feature_maps_cifar_test(imdb,net)
% -------------------------------------------------------------------------
% Seperate the Training and Testing Images
% -------------------------------------------------------------------------
    training_images=imdb.images.data(:,:,:,imdb.images.set == 1);
    training_labels=imdb.images.label(1,imdb.images.set == 1)';
    testing_images=imdb.images.data(:,:,:,imdb.images.set == 2);
    testing_labels=imdb.images.label(1,imdb.images.set == 2)';
    size_train = size(training_images);
    size_test=size(testing_images);
    
    training_feature_maps=zeros(size_train(4),100);
    
    testing_feature_maps=zeros(size_test(4),100);    
    
% -------------------------------------------------------------------------
% Computing Feature Maps for Training Images using the Trained Network
% given as input
% -------------------------------------------------------------------------
    for i=1:size_train(4)
        image=im2single(training_images(:,:,:,i));
        res=vl_simplenn(net,image);
        training_feature_maps(i,:)=reshape(res(9).x,[1,100]);        
    end
   
% -------------------------------------------------------------------------
% Computing Feature Maps for Testing Images using the Trained Network
% given as input
% -------------------------------------------------------------------------
    for i=1:size_test(4)
        image=im2single(testing_images(:,:,:,i));
        res=vl_simplenn(net,image);
        testing_feature_maps(i,:)=reshape(res(9).x,[1,100]);
    end
end