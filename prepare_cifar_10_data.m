function [ new_imdb ] = prepare_cifar_10_data()
% -------------------------------------------------------------------------
% Files having the Testing and Training Data
% -------------------------------------------------------------------------
    files={'cifar-10-batches-mat/data_batch_1.mat','cifar-10-batches-mat/data_batch_2.mat','cifar-10-batches-mat/data_batch_3.mat',
        'cifar-10-batches-mat/data_batch_4.mat','cifar-10-batches-mat/data_batch_5.mat','cifar-10-batches-mat/test_batch.mat'};
    
    %files={'cifar-10-batches-mat/data_batch_1.mat','cifar-10-batches-mat/test_batch.mat'};
    %imdb.meta = load('cifar-10-batches-mat/batches.meta.mat');
    
    
    no_of_files=size(files);
% -------------------------------------------------------------------------
% Iterating over each file
% -------------------------------------------------------------------------
    no_of_images = 0;
    for file_no=1:6%no_of_files(2)
        %Loading corresponding file
        file=char(files(file_no));
        imdb.images = load(file);  
        
        %No of images in file
        dim = size(imdb.images.data);
        
        %iterating over each image in the file
        for i = 1:dim(1)
            %creating a new database of images
            new_imdb.images.data(:,:,:,no_of_images+1) = reshape(imdb.images.data(i,:),[32,32,3]);
            
            %Adding Set as 2 for Test Batch and Set as 1 for Training Batches
            if strcmp(file,'cifar-10-batches-mat/test_batch.mat') == 1                
                new_imdb.images.set(1,no_of_images+1) = 2;
            else
                new_imdb.images.set(1,no_of_images+1) = 1;
            end
            
            %Setting ID,label and updating index
            new_imdb.images.id(1,no_of_images+1) = no_of_images+1;
            new_imdb.images.label(1,no_of_images+1)=imdb.images.labels(i,1);
            no_of_images = no_of_images + 1;
        end
    end
end

