function [ data,labels ] = prepare_svm_training_data( net )

    %Assigning Network and getting input data
    %net=initializeCIFARFeatureMap(20,20,20);
    svm_imdb = prepare_cifar_10_data;
    
    % -------------------------------------------------------------------------
    %  Getting Training Data
    % -------------------------------------------------------------------------
    svm_imdb.training_images=svm_imdb.images.data(:,:,:,svm_imdb.images.set==1);
    svm_imdb.training_labels=svm_imdb.images.label(svm_imdb.images.set==1);
    
    % -------------------------------------------------------------------------
    %  Allocating Space for SVM Training Data
    % -------------------------------------------------------------------------
    training_dim = size(svm_imdb.training_images);
    data.training_images=zeros(training_dim(4),10);
    labels.training_labels=zeros(training_dim(4),1);
    
    % -------------------------------------------------------------------------
    %  Getting Testing Data
    % -------------------------------------------------------------------------
    
    svm_imdb.testing_images=svm_imdb.images.data(:,:,:,svm_imdb.images.set==2);
    svm_imdb.testing_labels=svm_imdb.images.label(svm_imdb.images.set==2);
    
    % -------------------------------------------------------------------------
    %  Allocating Space for SVM Testing Data
    % -------------------------------------------------------------------------    
    testing_dim = size(svm_imdb.testing_images);
    data.testing_images = zeros(testing_dim(4),10);
    labels.testing_labels= zeros(testing_dim(4),1);
    
    
    % -------------------------------------------------------------------------
    %  Computing Feature Maps for Training Data and assigning Training data
    %  and labels
    % -------------------------------------------------------------------------
    dim=size(svm_imdb.training_images);
    training_data_index=0;
    for i =1:dim(4)
        image = svm_imdb.training_images(:,:,:,i);
        image=im2single(image);
        %computes feature map
        res = vl_simplenn(net, image);
        
        data.training_images(training_data_index+1,:) = reshape(res(end).x , [1,10]);
        training_data_index=training_data_index+1;
    end
    labels.training_labels = svm_imdb.training_labels';
    % -------------------------------------------------------------------------
    %  Computing Feature Maps for Testing Data and assigning Testing data
    %  and labels
    % -------------------------------------------------------------------------
    dim=size(svm_imdb.testing_images);
    testing_data_index=0;
    for i =1:dim(4)
        image = svm_imdb.testing_images(:,:,:,i);
        image=im2single(image);
        %computes feature map
        res = vl_simplenn(net, image);
        
        data.testing_images(testing_data_index+1,:) = reshape(res(end).x , [1,10]);
        testing_data_index = testing_data_index+1;
    end
    labels.testing_labels = svm_imdb.testing_labels';
end

