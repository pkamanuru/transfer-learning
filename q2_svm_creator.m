%Get Input
imdb=prepare_cifar_10_data;
%Train the network with the input
net=cifar_10_q2;

% -------------------------------------------------------------------------
% Compute feature maps using the Trained Network
% -------------------------------------------------------------------------
[testing_feature_maps,testing_labels,training_feature_maps,training_labels] =  get_feature_maps_cifar_test(imdb,net);

% -------------------------------------------------------------------------
% Train SVM Model using LibLinear
% -------------------------------------------------------------------------
model=train(training_labels,sparse(training_feature_maps),'-s 4 -c 10');

% -------------------------------------------------------------------------
% Predict the Output on both the Training and Testing Data Sets using the
% model
% -------------------------------------------------------------------------
x=predict(training_labels,sparse(training_feature_maps),model);
y=predict(testing_labels,sparse(testing_feature_maps),model);


% -------------------------------------------------------------------------
% Train SVM Model using LibSVM
% -------------------------------------------------------------------------
training_labels=double(training_labels);
training_feature_maps=double(training_feature_maps);
model_non_linear = svmtrain(training_labels,training_feature_maps,'-t 0 -c 20');

% -------------------------------------------------------------------------
% Predict the Output on both the Training and Testing Data Sets using the
% model
% -------------------------------------------------------------------------
x_non_linear=svmpredict(training_labels,training_feature_maps,model_non_linear);
y_non_linear=svmpredict(testing_labels,testing_feature_maps,model_non_linear);