data , labels = prepare_svm_training_data();
svm_struct=svmtrain(double(data.training_images),double(labels.training_labels),'-s 0 -c 1 -g 0.07 -b 1');
test_labels_output=svmclassify(svm_data,data.testing_images);

svm_struct=fitcsvm(data.training_images,labels.training_labels);
test_labels_output=svmclassify(svm_data,data.testing_images);
dim = size(test_labels_output);
no_of_matches=0;
for i=1:dim(1)
    if ( test_labels_output(i,1) == labels.testing_labels(i,1) )
        no_of_matches = no_of_matches +1;
    end
end